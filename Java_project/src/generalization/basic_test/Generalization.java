package generalization.basic_test;

/**
 * Created by Sasha on 10/14/2017.
 */
public class Generalization<T extends Number> {
    T object;
    Generalization(T obj){object=obj;}

    int returnInteger(){
        return object.intValue();
    }

    int returnGreater(Generalization<T> objectType){
        if (object.intValue()>=objectType.object.intValue()) return object.intValue();
        else return objectType.object.intValue();
    }
}
