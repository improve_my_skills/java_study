package enum_package.enum_basic_rule;

public enum Car {
    TOYTA(4), SKODA(2), BMW(5), OPEL(1);

    private int cost;

    Car(int cost){this.cost=cost;}


    public int returnCost(){
      return cost;
    }
}
