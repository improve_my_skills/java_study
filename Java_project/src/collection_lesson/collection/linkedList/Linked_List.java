package collection_lesson.collection.linkedList;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Sasha on 10/15/2017.
 */
public class Linked_List {

    // Fill out LinkedList with data from 0 till 5
    LinkedList<String> filloutLinkedList() {
        LinkedList<String> list = new LinkedList<String>();
        list.add("S");
        list.add("a");
        list.add("s");
        list.add("h");
        list.add("a");
        return list;
    }

    void printList(LinkedList<String> listType){
        for(String element:listType){
            System.out.println(element);
        }
    }

    // All kind of remove method;
    void removeMethods(LinkedList<String> listLinked){
        listLinked.remove("h");
        listLinked.removeFirst();
        listLinked.removeLast();
        printList(listLinked);
    }

    //Change list value
    void changeListValue(LinkedList<String> listLinked){
        listLinked.set(1,"o");
        printList(listLinked);
    }

    //Replace a on Good from Sasha
    void replaceA(LinkedList<String> linkedList){
        for(String letter:linkedList){
            if(letter.equals("a")){
              int a=linkedList.indexOf(letter);
              System.out.println(a);
              linkedList.set(a, "good");
            }
        }
        printList(linkedList);
    }
}
