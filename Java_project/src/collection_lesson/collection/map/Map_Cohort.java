package collection_lesson.collection.map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Sasha on 10/16/2017.
 */
public class Map_Cohort {


    // Fill out HashMap
    public HashMap<String, Double> fillHashMap(){
        HashMap<String, Double> hashMap=new HashMap<>();
        hashMap.put("Sasha",15650.0);
        hashMap.put("Diana", 950.6);
        hashMap.put("Vlad", 700.3);
        return hashMap;
    }

    // print Value of Map using interface Map.Entry<>
    public void printMap(HashMap<String, Double> hashMap){
        Set<Map.Entry<String, Double>> set=hashMap.entrySet();
        for (Map.Entry<String, Double> value:set){
            System.out.println(value.getKey()+" and "+value.getValue());
        }
    }
}
