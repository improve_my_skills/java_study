package collection_lesson.collection.array_list;

/**
 * Created by Sasha on 10/14/2017.
 */
public class Main_Array_List {
    public static void main(String args[]){
        Array_List array_list=new Array_List();
        //1) ArrayList to String
        array_list.transformToArray(array_list.filloutArrayList());
        // 2) In case using add method with index, then value that were in index position up on one index, not delete from list
        array_list.printList(array_list.checkAddMethod(array_list.filloutArrayList()));
        // Change element with se method
        array_list.changeValueWithSetMethod(array_list.filloutArrayList());
    }
}
