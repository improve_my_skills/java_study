package collection_lesson.collection.array_list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sasha on 10/15/2017.
 */
public class Array_List {

    // Fill out arrayList with data from 0 till 5
    ArrayList<Integer> filloutArrayList() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i <= 5; i++) list.add(i);
        return list;
    }

    // Transform and print data from arrayList to array
    public void transformToArray(ArrayList<Integer> arrayList){
        Integer massiv[]=new Integer[arrayList.size()];
        massiv=arrayList.toArray(massiv);
        for (Integer value:massiv)System.out.println("Massiv value= "+value);
    }

    // In case using add method with index, then value that were in index position up on one index, not delete from list
    ArrayList<Integer> checkAddMethod(ArrayList<Integer> object) {
        object.add(2,150);
        return object;
    }

    void printList(ArrayList<Integer> listType) {

        for (Integer element : listType) {
            System.out.println(element);
        }
    }

    void changeValueWithSetMethod(ArrayList<Integer> arrayList){
        arrayList.set(2,4444);
        printList(arrayList);

    }
}
