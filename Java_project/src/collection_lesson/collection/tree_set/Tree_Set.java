package collection_lesson.collection.tree_set;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by Sasha on 10/15/2017.
 */
public class Tree_Set {

    // Fill out TreeSet with data from 0 till 5
    TreeSet<String> filloutTreeSet() {
        TreeSet<String> list = new TreeSet<String>();
        list.add("S");
        list.add("a");
        list.add("s");
        list.add("h");
        list.add("a");
        return list;
    }

    void printList(TreeSet<String> listType) {

        for (String element : listType) {
            System.out.println(element);
        }
    }
}
