package collection_lesson.collection.comparator;

import java.util.Comparator;

/**
 * Created by Sasha on 10/18/2017.
 */
public class MyComp implements Comparator<String> {

    public int compare(String o1,String  o2){
        int i,j,k;
        String aStr, bStr;
        aStr=o1;
        bStr=o2;
        i=aStr.lastIndexOf(' ');
        j=bStr.lastIndexOf(' ');
        k=aStr.substring(i).compareTo(bStr.substring(j));
        if(k==0)return  aStr.compareTo(bStr);
        else return k;
    }
}
