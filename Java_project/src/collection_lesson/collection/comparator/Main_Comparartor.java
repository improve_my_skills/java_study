package collection_lesson.collection.comparator;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Sasha on 10/18/2017.
 */
public class Main_Comparartor {
    public static void main(String args[]) {
        TreeMap<String, Double> tm=new TreeMap<String,Double>(new MyComp());
        // Put elements in Cart;
        tm.put("Sasha Kalykhan", new Double(15.5));
        tm.put("Diana Mazuro", new Double(9.5));
        tm.put("Vlad Kalykhan", new Double(6));

        Set<Map.Entry<String,Double>> set=tm.entrySet();

        for(Map.Entry<String,Double> me:set){
            System.out.println(me.getKey()+" + "+ me.getValue());
        }
    }



}
