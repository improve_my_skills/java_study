package collection_lesson.collection.iterator.iterator_object;

/**
 * Created by Sasha on 10/15/2017.
 */
public class FullName {
    private String firstname;
    private String lastName;
     FullName(String firstName1, String lastName1){
         this.firstname=firstName1;
         this.lastName=lastName1;
     }

    public String getFirstname() {
        return firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
