package collection_lesson.collection.iterator.iterator_object;

import collection_lesson.collection.array_list.Array_List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Created by Sasha on 10/15/2017.
 */
public class Main_Iterator_Object {
    public static void main(String args[]){
        ArrayList<FullName> list=new ArrayList<FullName>();
        list.add(new FullName("Sasha", "Kalykhan"));
        list.add(new FullName("Diana", "Kalykhan"));
        list.add(new FullName("Lerka", "Mazuro"));
        ListIterator<FullName> iter=list.listIterator();
        while (iter.hasNext()){
            if(iter.next().getFirstname().equals("Sasha")) iter.set(new FullName("Alexander", "Kalykhan"));
        }

        ListIterator<FullName> iter1=list.listIterator();
        while (iter1.hasNext()){
            int a=iter1.nextIndex();
            System.out.println("First Name is "+iter1.next().getFirstname()+" and LastName is "+list.get(a).getLastName());
        }


    }
}
