package collection_lesson.collection.iterator;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Sasha on 10/15/2017.
 */
public class Iterator_Cohort {

    // Fill out LinkedList with data from 0 till 5
    LinkedList<String> filloutLinkedList() {
        LinkedList<String> list = new LinkedList<String>();
        list.add("S");
        list.add("a");
        list.add("s");
        list.add("h");
        list.add("a");
        return list;
    }

    void printList(LinkedList<String> listType){
        for(String element:listType){
            System.out.println(element);
        }
    }

    // Go through List with Iterator and delete all a
    void goWithIteratorAndDeleteA(LinkedList<String> linkedList){
        Iterator<String> itr=linkedList.iterator();
        while (itr.hasNext()){
            if(itr.next().equals("a"))itr.remove();
        }
        printList(linkedList);
    }
}
